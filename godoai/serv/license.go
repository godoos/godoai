/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package serv

import (
	"encoding/base64"
	"encoding/json"
	"godoai/libs"
	"net/http"
)

func GetLicenseHandle(w http.ResponseWriter, r *http.Request) {
	lineseInfo, err := libs.GetSystemInfo()
	if err != nil {
		libs.Error(w, "failed to generate Linese info.")
		return
	}
	w.WriteHeader(http.StatusOK) // 返回200状态码
	libs.Success(w, lineseInfo, "sucess")
}
func SetLicenseHandler(w http.ResponseWriter, r *http.Request) {
	type LicenseCode struct {
		LicenseCode string `json:"licenseCode"`
	}
	var req LicenseCode
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		libs.Error(w, "first Decode request body error")
		return
	}
	// Base64解码
	plaintext, err := base64.StdEncoding.DecodeString(req.LicenseCode)
	if err != nil {
		libs.Error(w, "base64 decode failed")
		return
	}
	// 反序列化JSON为UserOsInfo
	var licenseInfo libs.CheckLicenseInfo
	if err := json.Unmarshal(plaintext, &licenseInfo); err != nil {
		libs.Error(w, "反序列化JSON错误:"+err.Error())
		return
	}
	if !libs.VerifySystem(licenseInfo) {
		libs.Error(w, "licenseCode is invalid")
		return
	}
	err = libs.SetConfigByName("license", licenseInfo)
	if err != nil {
		libs.Error(w, "set licenseCode failed")
		return
	}
	w.WriteHeader(http.StatusOK) // 返回200状态码
	libs.Success(w, nil, "sucess")
}
