/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package serv

import (
	"fmt"
	"godoai/libs"
	"net/http"
)

// pingHandler 检查服务是否运行正常
func PingHandle(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK) // 返回200状态码
	libs.Success(w, "", "Pong! Service is running.")
}
func HomeHandle(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK) // 返回200状态码
	fmt.Fprintln(w, "Service is running.")
}
func (s *Server) RestartHandle(w http.ResponseWriter, r *http.Request) {
	// 尝试重启服务
	if err := s.Restart(); err != nil {
		http.Error(w, fmt.Sprintf("Failed to restart service: %v", err), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Service restarted")
}
