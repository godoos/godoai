/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sd

import (
	"fmt"
	"godoai/libs"
	"path/filepath"
	"runtime"
)

func GetRuner() (string, error) {
	baseDir, err := GetRunDir()
	if err != nil {
		return "", err
	}

	runerFileName := "sdlibs"
	if runtime.GOOS == "windows" {
		runerFileName = runerFileName + ".exe"
	}
	runerFile := filepath.Join(baseDir, runerFileName)

	if !libs.PathExists(runerFile) {
		// Write the content to the file
		// if err := os.WriteFile(runerFile, embeddedSdlibs, 0755); err != nil {
		// 	return "", fmt.Errorf("failed to write file: %w", err)
		// }
		return "", fmt.Errorf("runer file not found")
	}
	return runerFile, nil

}
