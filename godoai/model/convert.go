/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package model

import (
	"fmt"
	"godoai/libs"
	"net/http"
	"os"
)

func ConvertOllama(w http.ResponseWriter, r *http.Request, req ReqBody) {
	modelFile := "FROM " + req.Paths[0] + "\n"
	modelFile += `TEMPLATE """` + req.Info["template"].(string) + `"""`
	if parameters, ok := req.Info["parameters"].([]interface{}); ok {
		for _, param := range parameters {
			if strParam, ok := param.(string); ok {
				modelFile += "\nPARAMETER " + strParam
			} else {
				// 处理非字符串的情况，根据需要可以选择忽略或报告错误
				fmt.Fprintf(os.Stderr, "Unexpected parameter type: %T\n", param)
			}
		}
	}

	url := libs.GetOllamaUrl() + "/api/create"
	postParams := map[string]string{
		"name":      req.Model,
		"modelfile": modelFile,
	}
	ForwardHandler(w, r, postParams, url, "POST")
	modelDir, err := GetModelDir(req.Model)
	if err != nil {
		libs.Error(w, "GetModelDir")
		return
	}

	// modelFilePath := filepath.Join(modelDir, "Modelfile")
	// if err := os.WriteFile(modelFilePath, []byte(modelFile), 0644); err != nil {
	// 	ErrMsg("WriteFile", err, w)
	// 	return
	// }
	err = os.RemoveAll(modelDir)
	if err != nil {
		libs.Error(w, "Error removing directory")
		return
	}
}
