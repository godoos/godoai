/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package model

import (
	"bytes"
	"encoding/json"
	"godoai/libs"
	"io"
	"log"
	"net/http"
)

func ForwardHandler(w http.ResponseWriter, r *http.Request, reqBody interface{}, url string, method string) {
	payloadBytes, err := json.Marshal(reqBody)
	if err != nil {
		libs.Error(w, "Error marshaling payload")
		return
	}
	// 创建POST请求，复用原始请求的上下文（如Cookies）
	req, err := http.NewRequestWithContext(r.Context(), method, url, bytes.NewBuffer(payloadBytes))
	if err != nil {
		libs.Error(w, "Failed to create request")
		return
	}
	req.Header.Set("Content-Type", "application/json")

	// 发送请求
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		libs.Error(w, "Failed to send request")
		return
	}
	defer resp.Body.Close()
	// 将外部服务的响应内容原封不动地转发给客户端
	for k, v := range resp.Header {
		for _, value := range v {
			w.Header().Add(k, value)
		}
	}
	w.WriteHeader(resp.StatusCode)
	//log.Printf("resp.Body: %v", resp.Body)
	_, err = io.Copy(w, resp.Body)
	if err != nil {
		// 如果Copy过程中出错，尝试发送一个错误响应给客户端
		http.Error(w, "Error forwarding response", http.StatusInternalServerError)
		log.Printf("Error forwarding response body: %v", err)
		return
	}

}
