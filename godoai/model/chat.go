/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package model

import (
	"encoding/json"
	"godoai/libs"
	"net/http"
)

func ChatHandler(w http.ResponseWriter, r *http.Request) {
	url := libs.GetOllamaUrl() + "/v1/chat/completions"
	var request interface{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		libs.Error(w, err.Error())
		return
	}
	ForwardHandler(w, r, request, url, "POST")
}
func EmbeddingHandler(w http.ResponseWriter, r *http.Request) {
	url := libs.GetOllamaUrl() + "/api/embeddings"
	var request interface{}
	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		libs.Error(w, err.Error())
		return
	}
	ForwardHandler(w, r, request, url, "POST")
}
