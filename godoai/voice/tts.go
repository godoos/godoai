/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package voice

import (
	"encoding/base64"
	"encoding/json"
	"godoai/libs"
	"net/http"
	"os"
	"path/filepath"
)

type TtsRequest struct {
	Model  string    `json:"model"`
	Text   string    `json:"text"`
	Path   string    `json:"path"`
	Sid    int       `json:"sid"`
	Params ReqParams `json:"params"`
}
type TtsReponse struct {
	Txt string `json:"txt"`
}

func TtsHandler(w http.ResponseWriter, r *http.Request) {
	var req TtsRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		libs.Error(w, err.Error())
		return
	}
	voicePath, err := libs.GetVoiceDir()
	if err != nil {
		libs.Error(w, err.Error())
		return
	}
	// 生成随机文件名并保留原扩展名
	randomName := libs.GenerateRandomString(10) + ".wav"
	req.Path = filepath.Join(voicePath, randomName)
	if err := Txt2voc(req); err != nil {
		libs.Error(w, err.Error())
		return
	}
	txt, err := WavToBase64(req.Path)
	if err != nil {
		libs.Error(w, err.Error())
		return
	}
	res := ResUpload{
		Txt: txt,
	}
	libs.Success(w, res, "success")

}
func WavToBase64(filename string) (string, error) {
	// 读取文件内容
	data, err := os.ReadFile(filename)
	if err != nil {
		return "", err
	}

	// 将文件内容编码为Base64字符串
	base64Str := base64.StdEncoding.EncodeToString(data)

	return base64Str, nil
}
