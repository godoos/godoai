/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chromem

import (
	"context"
	"reflect"
	"testing"
)

func TestDocument_New(t *testing.T) {
	ctx := context.Background()
	id := "test"
	metadata := map[string]string{"foo": "bar"}
	vectors := []float32{-0.40824828, 0.40824828, 0.81649655} // normalized version of `{-0.1, 0.1, 0.2}`
	content := "hello world"
	embeddingFunc := func(_ context.Context, _ string) ([]float32, error) {
		return vectors, nil
	}

	tt := []struct {
		name          string
		id            string
		metadata      map[string]string
		vectors       []float32
		content       string
		embeddingFunc EmbeddingFunc
	}{
		{
			name:          "No embedding",
			id:            id,
			metadata:      metadata,
			vectors:       nil,
			content:       content,
			embeddingFunc: embeddingFunc,
		},
		{
			name:          "With embedding",
			id:            id,
			metadata:      metadata,
			vectors:       vectors,
			content:       content,
			embeddingFunc: embeddingFunc,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// Create document
			d, err := NewDocument(ctx, id, metadata, vectors, content, embeddingFunc)
			if err != nil {
				t.Fatal("expected no error, got", err)
			}
			// We can compare with DeepEqual after removing the embedding function
			d.Embedding = nil
			exp := Document{
				ID:       id,
				Metadata: metadata,
				Content:  content,
			}
			if !reflect.DeepEqual(exp, d) {
				t.Fatalf("expected %+v, got %+v", exp, d)
			}
		})
	}
}
