/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package chromem_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"slices"
	"strings"
	"testing"

	chromem "godoai/knowledge/chromemDB/chromem"
)

type openAIResponse struct {
	Data []struct {
		Embedding []float32 `json:"embedding"`
	} `json:"data"`
}

func TestNewEmbeddingFuncOpenAICompat(t *testing.T) {
	apiKey := "secret"
	model := "model-small"
	baseURLSuffix := "/v1"
	input := "hello world"

	wantBody, err := json.Marshal(map[string]string{
		"input": input,
		"model": model,
	})
	if err != nil {
		t.Fatal("unexpected error:", err)
	}
	wantRes := []float32{-0.40824828, 0.40824828, 0.81649655} // normalized version of `{-0.1, 0.1, 0.2}`

	// Mock server
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Check URL
		if !strings.HasSuffix(r.URL.Path, baseURLSuffix+"/embeddings") {
			t.Fatal("expected URL", baseURLSuffix+"/embeddings", "got", r.URL.Path)
		}
		// Check method
		if r.Method != "POST" {
			t.Fatal("expected method POST, got", r.Method)
		}
		// Check headers
		if r.Header.Get("Authorization") != "Bearer "+apiKey {
			t.Fatal("expected Authorization header", "Bearer "+apiKey, "got", r.Header.Get("Authorization"))
		}
		if r.Header.Get("Content-Type") != "application/json" {
			t.Fatal("expected Content-Type header", "application/json", "got", r.Header.Get("Content-Type"))
		}
		// Check body
		body, err := io.ReadAll(r.Body)
		if err != nil {
			t.Fatal("unexpected error:", err)
		}
		if !bytes.Equal(body, wantBody) {
			t.Fatal("expected body", wantBody, "got", body)
		}

		// Write response
		resp := openAIResponse{
			Data: []struct {
				Embedding []float32 `json:"embedding"`
			}{
				{Embedding: wantRes},
			},
		}
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(resp)
	}))
	defer ts.Close()
	baseURL := ts.URL + baseURLSuffix

	f := chromem.NewEmbeddingFuncOpenAICompat(baseURL, apiKey, model, nil)
	res, err := f(context.Background(), input)
	if err != nil {
		t.Fatal("expected nil, got", err)
	}
	if slices.Compare(wantRes, res) != 0 {
		t.Fatal("expected res", wantRes, "got", res)
	}
}
