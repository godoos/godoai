/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package libs

// SplitText splits a text into multiple text.
func SplitTokenText(text string, contextLength int) ([]string, error) {

	texts := splitText(text, contextLength)

	return texts, nil
}
func splitText(text string, ChunkSize int) []string {
	splits := make([]string, 0)
	Texts := SplitText(text, ChunkSize)
	// for _, txt := range Texts {
	// 	splits = append(splits, txt)
	// }
	splits = append(splits, Texts...)
	return splits

}

/*
func spText(text string, tk *tokenizers.Tokenizer) string {
	//splits := make([]string, 0)
	_, tokens := tk.Encode(text, false)
	//fmt.Printf("tokens==%v", tokens)

	if len(tokens) == 0 {
		return "" // 如果没有tokens，直接返回空切片
	}
	//re := regexp.MustCompile(`\[[Uu][Nn][Kk]\]|\[[Pp][Aa][Dd]\]`)
	//str := re.ReplaceAllString(strings.Join(tokens, ""), "")
	var str string
	for _, token := range tokens {
		if token == "[UNK]" || token == "[PAD]" {
			continue
		}
		str += token
	}
	str = strings.ReplaceAll(str, "##", "")
	return str
}
*/
