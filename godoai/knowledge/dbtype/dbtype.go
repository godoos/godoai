/*
 * GodoAI - A software focused on localizing AI applications
 * Copyright (C) 2024 https://godoos.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dbtype

type Database interface {
	Create(name string, model string) (CreateResponse, error)
	List() ([]string, error)
	Delete(name string) error
	Add(name string, model string, files []string) error
	Ask(name string, model string, message string) ([]AskResponse, error)
	DeleteFile(name string, model string, file string) error
}
type EmbedConifig struct {
	ApiUrl        string `json:"apiUrl"`
	ApiKey        string `json:"apiKey"`
	ApiType       string `json:"apiType"`
	ContextLength int    `json:"contextLength"`
}
type DbConfig struct {
	Type      string       `json:"type"`
	ApiUrl    string       `json:"apiUrl"`
	ApiKey    string       `json:"apiKey"`
	Embedding EmbedConifig `json:"embedding"`
}
type DbFactory struct {
	DB Database
}
type ConfigParams struct {
	Config DbConfig `json:"config"`
}
type CreateParams struct {
	Name   string   `json:"name"`
	Model  string   `json:"model"`
	Config DbConfig `json:"config"`
}
type DeleteParams struct {
	Name   string   `json:"name"`
	Config DbConfig `json:"config"`
}
type Files struct {
	Name string `json:"name"`
	Path string `json:"path"`
}
type AddParams struct {
	Name   string   `json:"name"`
	Model  string   `json:"model"`
	Files  []string `json:"files"`
	Config DbConfig `json:"config"`
}
type DeleteFilearams struct {
	Name   string   `json:"name"`
	Model  string   `json:"model"`
	File   string   `json:"file"`
	Config DbConfig `json:"config"`
}
type AskParams struct {
	Name    string   `json:"name"`
	Model   string   `json:"model"`
	Message string   `json:"message"`
	Config  DbConfig `json:"config"`
}
type Article struct {
	Text     string `json:"text"`
	File     string `json:"file"`
	Category string `json:"category"`
}
type DocumentParams struct {
	ID        string
	Metadata  map[string]string
	Embedding []float32
	Content   string
}
type AskResponse struct {
	ID         string            `json:"id"`
	Metadata   map[string]string `json:"metadata"`
	Embedding  []float32         `json:"embedding"`
	Content    string            `json:"content"`
	Similarity float32           `json:"similarity"`
}
type CreateResponse struct {
	Name string `json:"name"`
	Id   string `josn:"id"`
}
